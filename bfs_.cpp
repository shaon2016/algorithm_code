#include<bits/stdc++.h>
using namespace std;

int graph[20][20], n, color[20], d[20],pai[20], u, v;
queue <int> q;

void bfs(int s)
{
    for(u = 1; u <= n; u++) {
        color[u] = 0;
        d[u] = 99999999;
        pai[u] = 0;
    }

    color[s] = 1;
    d[s] = 0;
    pai[s] = 0;

    q.push(s);

    while(!q.empty()) {
        u = q.front();

        q.pop();

        for(v = 1; v <= n; v++) {
             // directed graph er jonne    graph[u][v]==1 eta rekhe || graph [v][u] == 1 eta bad dite hbe

            if((graph[u][v]==1 || graph [v][u] == 1) && color[v] == 0) {
                color[v] = 1;
                d[v] = d[u] + 1;
                pai[v] = u;

                q.push(v);
            }
        }
        color[u] = 2;
    }

    cout<<"Discover time: ";
    for(int i = 1; i <= n; i++) {
        printf("%d ", d[i]);
    }

}

int main()
{
    printf("Enter the nodes: ");
    scanf("%d", &n);

    printf("\nEnter the adjacency matrix\n");

    for(u = 1; u <= n; u++) {
        for(v = 1; v <= n; v++) {
            scanf("%d", &graph[u][v]);
        }
    }

    bfs(2);
    return 0;
}
