#include<bits/stdc++.h>

using namespace std;
int main()
{
    int graph[30][30], n, degreeOfVertices[30] = {0};
 // not circle if p = 0
 // l = 1 means cycle
    int p = 0, l = 1;
    cout << "How many nodes: ";
    cin >> n;

    cout << endl;

    for(int i = 1; i <= n; i ++) {
        for( int j = 1; j <= n; j++) {
            cin >>graph[i][j];

            // row degreeOfVertices for finding circle in a graph
            // we will not count loop to determine the circle
            if(graph[i][j] == 2 && graph[i][j] == 0)
                continue;
            else degreeOfVertices[i] += graph[i][j];
        }
    }

    // not circle if p = 0

    for(int i = 1; i <= n; i ++) {
        if(degreeOfVertices[i] != 2) // not circle
            p = 0;
        else
            p = 1;
    }

    if(p == 1 && l == 1)
        cout << "Circle" << endl;
    else cout << "Not circle" <<endl;


    return 0;
}
