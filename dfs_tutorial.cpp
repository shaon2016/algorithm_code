#include<bits/stdc++.h>
#include<iostream>
using namespace std;

int graph[20][20], color[5], discover_time[10], finishing_time[10], pai[10];
int time_count, n;
/*
white = 0
grey = 1
black = 2

*/

void dfs_visit(int l)
{
    time_count++;
    discover_time[l] = time_count;
    color[l] = 1;

    for(int v = 1; v <= n; v++)
        if(  graph[l][v] == 1 && color[v] == 0)
            dfs_visit(v);

    color[l] = 2;
    time_count++;
    finishing_time[l] = time_count;

}


void dfs()
{
    for(int u = 1; u <= n; u++)
        color[u] = 0;


    time_count = 0;

    for(int u = 1; u <= n; u++)
        if(color[u] == 0)
            dfs_visit(u);
}



int main()
{
    printf("Enter number of nodes: ");
    scanf("%d", &n);

    printf("\nEnter adjacency matrix of the graph\n");

    for(int u = 1; u <= n; u++) {
        for(int v = 1; v <= n; v++) {
            scanf("%d", &graph[u][v]);
        }
    }

    dfs();

    printf("Nodes\tDiscover \tFinishing \n");

    for(int i = 1; i <= n; i++) {
        printf("%d\t  %d\t  %d\n", i, discover_time[i], finishing_time[i]);
    }

    return 0;
}
