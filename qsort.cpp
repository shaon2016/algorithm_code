#include<bits/stdc++.h>

using namespace std;


int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}

int main()
{
    int f[5] = {2,1,0,3,100};

    qsort (f, 5, sizeof(int), compare);

    for(int i = 0; i < 5; i++) {
        printf("%d\n",  f[i]);
    }

    return 0;
}

