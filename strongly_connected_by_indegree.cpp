#include<bits/stdc++.h>
using namespace std;

int graph[20][20], n, color[20], d[20],pai[20], u, v;
queue <int> q;
bool isAllVisitedForAllVertices = true;
void bfs(int s)
{
    for(u = 1; u <= n; u++) {
        color[u] = 0;
        d[u] = 99999999;
        pai[u] = 0;
    }

    color[s] = 1;
    d[s] = 0;
    pai[s] = 0;

    q.push(s);

    while(!q.empty()) {
        u = q.front();

        q.pop();

        for(v = 1; v <= n; v++) {
            if((graph[u][v]==1) && color[v] == 0) {
                color[v] = 1;
                d[v] = d[u] + 1;
                pai[v] = u;

                q.push(v);
            }
        }
        color[u] = 2;
    }


    for(int x = 1; x <= n; x++) {
        if(color[x] == 2)
            continue;
        else isAllVisitedForAllVertices = false;
    }

}

int main()
{
    printf("Enter the nodes: ");
    scanf("%d", &n);

    printf("\nEnter the adjacency matrix\n");

    for(u = 1; u <= n; u++) {
        for(v = 1; v <= n; v++) {
            scanf("%d", &graph[u][v]);
        }
    }

    for(int k = 1; k <= n; k++) {
        if(isAllVisitedForAllVertices == false) {
            cout<< "Not Strongly Connected";
            break;
        }

        bfs(k);
    }
    if(isAllVisitedForAllVertices == true)
            cout<< " Strongly Connected";

    return 0;
}
