#include<bits/stdc++.h>
#include<iostream>
using namespace std;

int graph[20][20], color[5], discover_time[10], finishing_time[10], pai[10];
int time_count, n;
int node[50];
/*
white = 0
grey = 1
black = 2

*/

int compare (const void * a, const void * b)
{
  return ( *(int*)a - *(int*)b );
}


void dfs_visit(int l)
{
    time_count++;
    discover_time[l] = time_count;
    color[l] = 1;

    for(int v = 0; v < n; v++)
        if(  graph[l][v] == 1 && color[v] == 0)
            dfs_visit(v);

    color[l] = 2;
    time_count++;
    finishing_time[l] = time_count;

     node[time_count] = l;

}


void dfs()
{
    for(int u = 0; u < n; u++)
        color[u] = 0;


    time_count = 0;

    for(int u = 0; u < n; u++)
        if(color[u] == 0)
            dfs_visit(u);
}



int main()
{
    printf("Enter number of nodes: ");
    scanf("%d", &n);

    printf("\nEnter adjacency matrix of the graph\n");

    for(int u = 0; u < n; u++) {
        for(int v = 0; v < n; v++) {
            scanf("%d", &graph[u][v]);
        }
    }

    dfs();


   qsort (finishing_time, n, sizeof(int), compare);

    cout << "Nodes\t" << "Finishing time" << endl;
    for(int i = n-1; i > -1; i--) {
        printf("%d\t%d\n",  node[finishing_time[i]],finishing_time[i]);
    }

    return 0;
}
