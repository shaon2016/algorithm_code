#include<bits/stdc++.h>

using namespace std;
int main()
{
    bool isThereAnyCenterOfVertex = false;
    int adjacencyOfAGraph[30][30], n, degreeOfVertices[30] = {0};
    int countingDegreeForOtherVertices = 0;

    cout << "How many nodes: ";
    cin >> n;

    cout << endl;

    for(int i = 1; i <= n; i++) {
        for( int j = 1; j <= n; j++) {
            cin >>adjacencyOfAGraph[i][j];

            // row degreeOfVertices for finding circle in a graph
            if(adjacencyOfAGraph[i][j] != 2 && adjacencyOfAGraph[i][j] != 0)
                degreeOfVertices[i] += adjacencyOfAGraph[i][j];
        }
    }

    // A wheel constitute with a center vertex
    // which connected with all the other vertices

    // Note
    // A center vertices must have total (number of nodes -1)
    // edge or degree to
    // connect with all the other node or vertices
// and other vertices connected with each other along
// with their center vertices
// so the must have to have 3 degree or 3 edge
// other wise they can't form a wheel

    for(int i = 1; i <= n; i++) {
        // checking other vertices number must be
        // equal to total number of vertices - 1
        if(degreeOfVertices[i] == 3)
            countingDegreeForOtherVertices++;
        if(degreeOfVertices[i] == (n-1)) // have a center
            isThereAnyCenterOfVertex = true;

    }


        // in 4 nodes all the vertices have 3 edge
    if(n==4)
        countingDegreeForOtherVertices--;



    if((isThereAnyCenterOfVertex == true) && (countingDegreeForOtherVertices == (n-1)))
        cout << "This graph is a wheel" << endl;
    else cout << "This graph is not a wheel" <<endl;


    return 0;
}
